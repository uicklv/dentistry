<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(\App\Record::class, function (Faker $faker) {

    $date = Carbon::now()->startOfWeek()->addDays(rand(0,6));
    $hourse = rand(9, 17);
    $time = new Carbon($hourse  . ':00:00');
    return [
        'id' => $faker->uuid,
        'dentist_id' =>\App\Dentist::inRandomOrder()->limit(1)->get()->first()->id,
        'user_id' =>\App\User::inRandomOrder()->limit(1)->get()->first()->id,
        'date' =>  $date,
        'time' => $time,
    ];
});
