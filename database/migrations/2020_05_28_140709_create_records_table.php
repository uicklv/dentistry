<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('dentist_id');
            $table->uuid('user_id');
            $table->date('date');
            $table->time('time');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('dentist_id')->references('id')->on('dentists');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
