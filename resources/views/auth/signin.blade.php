@extends('layout')

@section('title', 'Вход')

@section('content')
    <div class="row">
        <div class="col-9">
            <h5 class="mt-5">Вход</h5>
            <hr>
            <form action="{{route('signin.handle')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email"  value="{{old('email')}}">
                    @if($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('email') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="form-group" id="div_password">
                    <label for="password">Пароль:</label>
                    <input type="password"  name="password" class="form-control" id="password">
                    @if($errors->has('password'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('password') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Вход</button>
            </form>
        </div>
    </div>
@endsection
