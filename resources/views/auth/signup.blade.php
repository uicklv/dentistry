@extends('layout')

@section('title', 'Регистрация')

@section('content')
    <div class="row">
        <div class="col-9">
            <h5 class="mt-5">Регистрация</h5>
            <hr>
            <form action="{{route('signup.handle')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">ФИО (полностью):</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
                    @if($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('name') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="birthday">День рождения:</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{old('birthday')}}">
                    @if($errors->has('birthday'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('birthday') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email"  value="{{old('email')}}">
                    @if($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('email') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="form-group" id="div_password">
                    <label for="password">Пароль:</label>
                    <input type="password"  name="password" class="form-control" id="password">
                    @if($errors->has('password'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('password') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="form-group" id="div_password_confirmation">
                    <label for="password_confirmation">Подтверждение пароля:</label>
                    <input type="password"  name="password_confirmation" class="form-control" id="password_confirmation">
                    @if($errors->has('password_confirmation'))
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->get('password_confirmation') as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Отправить</button>
            </form>
        </div>
    </div>
@endsection
