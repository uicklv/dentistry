@extends('layout')

@section('title', 'Главная')

@section('content')
    <div class="row">
        <div class="col-9 ">
            <h3 class="mt-4">Dentist+</h3>
            <p>Лучшая стоматология в Украине...</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-9 ">
            <h4 class="mt-4">Наши специалисты:</h4>
            <hr>
        </div>
    </div>
        <div class="card-columns">
            @foreach($dentists as $dentist)
                <div class="card" style="width: 16rem;">
                    <div class="text-center mt-3"><i class="fas fa-user fa-5x" style="color: #a7a7ad;"></i></div>
                    <div class="card-body">
                        <h5 class="card-title">{{$dentist->name}}</h5>
                        <a href="{{route('dentist.show', ['dentist' => $dentist->id])}}" class="btn btn-primary">Записаться на приём</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://kit.fontawesome.com/c1fb75a26d.js" crossorigin="anonymous"></script>
@endpush

