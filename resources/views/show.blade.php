@extends('layout')

@section('title', 'Вход')

@section('content')
    <div class="row">
        <div class="col-9 ">
            <h4 class="mt-4">Раписание {{$dentist->name}} на текущую неделю:</h4>
            <hr>
            <div class="alert alert-warning" role="alert">
                Красным помечено занятое время
            </div>
            <table class="table table-bordered">
                <tr style="font-size:13px;">
                    <th>#</th>
                @foreach($thisWeek as $day)
                    <th scope="col">{{$day}}</th>
                @endforeach
                </tr>
                @foreach($times as $time)
                    <tr style="font-size:13px;">
                        <th scope="row">{{$time}}</th>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[0]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[1]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[2]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[3]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[4]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[5]) background-color:indianred;@endif @endforeach"></td>
                        <td style = "@foreach($records as $record) @if($record->time ==  $time && $record->date == $thisWeek[6]) background-color:indianred;@endif @endforeach"></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-9 ">
            <h4 class="mt-4">Записаться на приём:</h4>
            <hr>
            <div class="alert alert-primary" role="alert">
                Выберите свободную дату/время и  подтвердите запись.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-9">
            <form id="sendform">
                @csrf
                <div class="form-group">
                    <label for="date">Дата:</label>
                    <input type="date" class="form-control" id="date" name="date"  value="{{old('date')}}">
                    <div class="alert alert-danger" id="date_error" role="alert" style="display: none;"></div>
                </div>
                <div class="form-group" id="div_password">
                    <label for="time">Время:</label>
                    <input type="time"  name="time" class="form-control" id="time">
                    <div class="alert alert-danger" id="time_error" role="alert"  style="display: none;"></div>
                </div>
                <div class="alert alert-danger" id="message_error" role="alert"  style="display: none;"></div>
                <div class="alert alert-success" id="message_success" role="alert"  style="display: none;"></div>
                <button type="submit" id="send_data" class="btn btn-success mb-5">Запись</button>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function (){
            $('#send_data').click(function(e){
                e.preventDefault();
                var form = $(this).closest('#sendform').serialize() + '&dentist_id={{$dentist->id}}';
                $.ajax({
                    method: 'POST',
                    url: '{{route('record.store')}}',
                    data: form,
                    error: function(xhr) {
                        var response = jQuery.parseJSON(xhr.responseText);
                        if(response.errors){
                            for(var k in response.errors)
                            {
                                $('#' + k + '_error').html('');
                                document.getElementById(k).classList.add('is-invalid');
                                $('#' + k + '_error').css('display', 'block');
                                for(var i = 0; i < response.errors[k].length; i++) {
                                    $('#' + k + '_error').append(response.errors[k][i]);
                                }
                            }
                        }
                        if(response.error)
                        {
                            $('#message_error').css('display', 'block');
                            $('#message_error').append(response.error);
                        }
                    },
                    success: function(response, status, xhr) {
                        if(response.success)
                        {
                            $('#message_success').css('display', 'block');
                            $('#message_success').append(response.success);
                        }
                    }
                });
            });
        });
    </script>
@endpush
