<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/records', '\\' . \App\Http\Controllers\RecordController::class . '@store')
    ->name('record.store')
    ->middleware('auth');

Route::get('/', '\\' . \App\Http\Controllers\DentistController::class . '@index')
    ->name('home');

Route::get('/dentist/{dentist}', '\\' . \App\Http\Controllers\DentistController::class . '@show')
    ->name('dentist.show')
    ->middleware('auth');

//auth
Route::get('/signin', '\\' . \App\Http\Controllers\SigninController::class . '@index')
    ->name('signin.index')
    ->middleware('guest');

Route::post('/', '\\' . \App\Http\Controllers\SigninController::class . '@handle')
    ->name('signin.handle')
    ->middleware('guest');

Route::get('/signup', '\\' . \App\Http\Controllers\SignupController::class . '@index')
    ->name('signup.index')
    ->middleware('guest');

Route::post('/signup', '\\' . \App\Http\Controllers\SignupController::class . '@handle')
    ->name('signup.handle')
    ->middleware('guest');

Route::get('/logout', '\\' . \App\Http\Controllers\SignupController::class . '@logout')
    ->name('logout')
    ->middleware('auth');
