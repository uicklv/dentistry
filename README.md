1) Скопируйте .env.example и переименуйте его в .env. Впишите ваши данные в этот файл (данные БД и т.д.).
2) Запустите `composer install`.
3) Запустите `php artisan key:generate`. 
4) Запустите `php artisan migrate`.
5) Запустите `php artisan db:seed`.  
