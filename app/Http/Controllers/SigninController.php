<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SigninController extends Controller
{
    public function index()
    {
        return view('auth.signin');
    }

    public function handle(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8',
        ]);

        $data = $request->only(['email', 'password']);

        if (Auth::attempt($data))
        {
            return redirect()->route('home');
        }

    }
}
