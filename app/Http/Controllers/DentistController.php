<?php


namespace App\Http\Controllers;


use App\Dentist;
use Carbon\Carbon;

class DentistController extends Controller
{

    public function index()
    {
        $dentists = Dentist::all();
        return view('home', ['dentists' => $dentists]);
    }

    public function show(Dentist $dentist)
    {
        $records = $dentist->records()->get();
        $thisWeek = [];
            for ($i = 0; $i < 7; $i++)
            {
                $weekStartDate = Carbon::now()->startOfWeek();
                array_push($thisWeek, $weekStartDate->addDays($i)->format('Y-m-d'));
            }
        $times = ['09:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00',
            '17:00:00',];
        return view('show', ['dentist' => $dentist, 'records' => $records, 'thisWeek' => $thisWeek, 'times' => $times]);
    }

}
