<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class SignupController extends Controller
{
    public function index()
    {
        return view('auth.signup');
    }

    public function handle(Request $request)
    {
        $request->validate([
            'name' => 'required|min:6',
            'birthday' => 'required|date_format:Y-m-d',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);

        $data = $request->only(['name', 'birthday', 'email', 'password']);

        $user = new User();
        $user->id = Uuid::uuid4();
        $user->name = $data['name'];
        $user->birthday = $data['birthday'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();
        if (Auth::attempt($data))
        {
            return redirect()->route('home');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('signin.index');
    }
}
