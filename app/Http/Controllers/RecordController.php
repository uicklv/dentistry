<?php


namespace App\Http\Controllers;


use App\Dentist;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;

class RecordController extends Controller
{

    public function store(Request $request)
    {

        $times = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00',
            '17:00',];
        $request->validate([
            'dentist_id' => 'required|uuid',
            'date' => 'required|after:' . Carbon::now()->format('Y-m-d'),
            'time' => 'required|' . Rule::in($times),
            ]);
        $record = Record::where('dentist_id', '=' ,$request->get('dentist_id'))
            ->where('date', '=' ,$request->get('date'))
            ->where('time', '=' ,$request->get('time'));
        if ($record->count() > 0)
        {
            return response()->json(['error' => 'This date is reserved'], 422);
        }else {

            $record = new Record();
            $record->id = Uuid::uuid4();
            $record->dentist_id = $request->get('dentist_id');
            $record->user_id = Auth::user()->id;
            $record->date = $request->get('date');
            $record->time = $request->get('time');
            $record->save();
            return response()->json(['success' => 'You have successfully registered on ' . $request->get('date') .
            ' at ' . $request->get('time')], 200);
        }
    }
}
