<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Record extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'string';

    public function dentist()
    {
        return $this->belongsTo(\App\Dentist::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

}
